package com.galvanize.LastMinuteTravel.Activities;

import com.galvanize.LastMinuteTravel.Destinations.Destinations;

import javax.persistence.*;
import java.util.Set;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "activities")

public class Activities {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "activity")
    private String activity;

    @ManyToMany(mappedBy = "hasActivities")
    private Set<Destinations> has;

    public Activities(){}

    public Activities(String activity) {
        this.activity = activity;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
