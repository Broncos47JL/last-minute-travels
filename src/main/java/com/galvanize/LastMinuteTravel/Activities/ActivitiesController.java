package com.galvanize.LastMinuteTravel.Activities;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ActivitiesController {

    ActivitiesService activitiesService;

    public ActivitiesController(ActivitiesService activitiesService) {
        this.activitiesService = activitiesService;
    }

    @GetMapping("/activities")
    public ResponseEntity<ActivitiesList> getActivities() {
        ActivitiesList activitiesList;
        activitiesList = activitiesService.getActivities();
        return activitiesList.isEmpty() ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(activitiesList);
    }

}
