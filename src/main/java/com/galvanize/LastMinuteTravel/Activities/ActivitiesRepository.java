package com.galvanize.LastMinuteTravel.Activities;

import com.galvanize.LastMinuteTravel.Activities.Activities;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivitiesRepository extends JpaRepository<Activities, Long> {
}
