package com.galvanize.LastMinuteTravel.Activities;

import com.galvanize.LastMinuteTravel.Activities.Activities;
import com.galvanize.LastMinuteTravel.Activities.ActivitiesRepository;
import com.galvanize.LastMinuteTravel.Geography.GeographyList;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ActivitiesService {

    ActivitiesRepository activitiesRepository;

    public ActivitiesService(ActivitiesRepository activitiesRepository) {
        this.activitiesRepository = activitiesRepository;
    }

    public ActivitiesList getActivities() {
        return new ActivitiesList(activitiesRepository.findAll());
    }

    public Optional<Activities> findById(Long activities) {
        return activitiesRepository.findById(activities);
    }
}
