package com.galvanize.LastMinuteTravel.Activities;

import com.galvanize.LastMinuteTravel.Geography.Geography;
import com.galvanize.LastMinuteTravel.Geography.GeographyList;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ActivitiesList {

    private List<Activities> activities;

//=====================
// CONSTRUCTORS
//=====================

    public ActivitiesList() {
        this.activities = new ArrayList<>();
    }
    public ActivitiesList(List<Activities> activities) {
        this.activities = activities;
    }

//=====================
// METHODS
//=====================

    public List<Activities> getActivities() {
        return activities;
    }

    public void setActivities(List<Activities> activities) {
        this.activities = activities;
    }

    public boolean isEmpty(){
        return this.activities.isEmpty();
    }

    @Override
    public String toString() {
        return "ActivitiesList{" +
                "activities=" + activities +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivitiesList activitiesList = (ActivitiesList) o;
        return Objects.equals(activities, activitiesList.activities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(activities);
    }

}
