package com.galvanize.LastMinuteTravel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LastMinuteTravelApplication {

	public static void main(String[] args) {
		SpringApplication.run(LastMinuteTravelApplication.class, args);
	}

}
