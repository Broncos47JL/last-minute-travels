package com.galvanize.LastMinuteTravel.Destinations;
import com.galvanize.LastMinuteTravel.Activities.Activities;
import com.galvanize.LastMinuteTravel.Geography.Geography;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;


@Repository
public interface DestinationsRepository extends JpaRepository<Destinations, Long> {

//    @Query("SELECT destinations.city, geography.geography, activities.activity FROM destinations\n" +
//            "JOIN geojoindest ON destinations.id = geojoindest.city_id\n" +
//            "JOIN geography ON geojoindest.geo_id = geography.id\n" +
//            "JOIN actjoindest ON destinations.id = actjoindest.city_id\n" +
//            "JOIN activities ON actjoindest.act_id = activities.id\n" +
//            "WHERE geography.geography = :geography AND activities.activity = :activities;")
    List<Destinations> findByHasGeographyInAndHasActivitiesIn(Set<Geography> hasGeography, Set<Activities> hasActivities);
//    @Query("SELECT destinations.city, geography.geography FROM destinations\n" +
//            "JOIN geojoindest ON destinations.id = geojoindest.city_id\n" +
//            "JOIN geography ON geojoindest.geo_id = geography.id" +
//            "WHERE geography.geography = :geography;")
    List<Destinations> findByHasGeographyIn(Set<Geography> hasGeography);
//    @Query("SELECT destinations.city, activities.activity FROM destinations\n" +
//            "JOIN actjoindest ON destinations.id = actjoindest.city_id\n" +
//            "JOIN activities ON actjoindest.act_id = activities.id" +
//            "WHERE activities.activity = :activities;")
    List<Destinations> findByHasActivitiesIn(Set<Activities> hasActivities);
    Optional<Destinations> findByCity(String city);

}
