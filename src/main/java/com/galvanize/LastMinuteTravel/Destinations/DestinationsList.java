package com.galvanize.LastMinuteTravel.Destinations;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DestinationsList {

    private List<Destinations> destinations;

//=====================
// CONSTRUCTORS
//=====================

    public DestinationsList() {
        this.destinations = new ArrayList<>();
    }
    public DestinationsList(List<Destinations> destinations) {
        this.destinations = destinations;
    }

//=====================
// METHODS
//=====================

    public List<Destinations> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Destinations> destinations) {
        this.destinations = destinations;
    }

    public boolean isEmpty(){
        return this.destinations.isEmpty();
    }

    @Override
    public String toString() {
        return "DestinationsList{" +
                "destinations=" + destinations +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DestinationsList destinationsList = (DestinationsList) o;
        return Objects.equals(destinations, destinationsList.destinations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(destinations);
    }
}
