package com.galvanize.LastMinuteTravel.Destinations;

import com.galvanize.LastMinuteTravel.Activities.Activities;
import com.galvanize.LastMinuteTravel.Geography.Geography;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DestinationsService {

    DestinationsRepository destinationsRepository;
    public DestinationsService(DestinationsRepository destinationsRepository){
        this.destinationsRepository = destinationsRepository;
    }

    public DestinationsList getDestinations() {
        return new DestinationsList(destinationsRepository.findAll());
    }

    public DestinationsList getDestinationsByGeography(Set<Geography> hasGeography) {
      List<Destinations> destinations = destinationsRepository.findByHasGeographyIn(hasGeography);
        if (!destinations.isEmpty()) {
            return new DestinationsList(destinations);
        }
        return null;
    }

    public DestinationsList getDestinationsByActivity(Set<Activities> hasActivities) {
        List<Destinations> destinations = destinationsRepository.findByHasActivitiesIn(hasActivities);
        if (!destinations.isEmpty()) {
            return new DestinationsList(destinations);
        }
        return null;
    }

    public DestinationsList getDestinations(String city) {
        Optional<Destinations> destinations = destinationsRepository.findByCity(city);
//        if(!destinations.isEmpty()){
            return new DestinationsList(destinations.stream().collect(Collectors.toList()));
//        }
//        return null;
    }

    public DestinationsList getDestinations(Set<Geography> hasGeography, Set<Activities> hasActivities) {
        List<Destinations> destinations = destinationsRepository.findByHasGeographyInAndHasActivitiesIn(hasGeography, hasActivities);
        if(!destinations.isEmpty()) {
            return new DestinationsList(destinations);
        }
        return null;
    }

    public Destinations addDestinations(Destinations destinations) {
        return destinationsRepository.save(destinations);
    }

    public Destinations updateDestinations(String city, ArrayList<String> geography, ArrayList<String> activities){
        Optional<Destinations> oDestinations = destinationsRepository.findByCity(city);
        if(oDestinations.isPresent()){
            Destinations destinations = oDestinations.get();
            for (String geo : geography) {
                if (!destinations.getHasGeography().contains(geo)) {
                    oDestinations.get().addGeography(geo);
                }
            }
            for (String act : activities) {
                if(!destinations.getHasActivities().contains(act)){
                    oDestinations.get().addActivities(act);
                }
            }
            return destinationsRepository.save(oDestinations.get());
        }
        return null;
    }

    public void deleteDestination(String city) {
        Optional<Destinations> oDestinations = destinationsRepository.findByCity(city);
        if(oDestinations.isPresent()){
            destinationsRepository.delete(oDestinations.get());
        }else{
            throw new DestinationNotFoundException();
        }
    }
}
