package com.galvanize.LastMinuteTravel.Destinations;

import com.galvanize.LastMinuteTravel.Activities.Activities;
import com.galvanize.LastMinuteTravel.Activities.ActivitiesService;
import com.galvanize.LastMinuteTravel.Geography.Geography;
import com.galvanize.LastMinuteTravel.Geography.GeographyService;
import com.galvanize.LastMinuteTravel.Users.UpdateAttributesRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class DestinationsController {

    DestinationsService destinationsService;
    GeographyService geographyService;

    ActivitiesService activitiesService;

    public DestinationsController(DestinationsService destinationsService, GeographyService geographyService, ActivitiesService activitiesService) {
        this.destinationsService = destinationsService;
        this.geographyService = geographyService;
        this.activitiesService = activitiesService;
    }

    @GetMapping("/destinations")
    public ResponseEntity<DestinationsList> getDestinations(@RequestParam(required = false) Long geography,
                                                            @RequestParam(required = false) Long activities) {

        Set<Geography> hasGeography = new HashSet<Geography>();
        Set<Activities> hasActivities = new HashSet<Activities>();

        DestinationsList destinationsList;
        if (geography == null && activities == null) {
            destinationsList = destinationsService.getDestinations();
        } else if (geography != null && activities == null) {
            Optional<Geography> geo = geographyService.findById(geography);
            hasGeography = geo.map(Set::of).orElse(Collections.emptySet());
            destinationsList = destinationsService.getDestinationsByGeography(hasGeography);
        } else if (geography == null && activities != null) {
            Optional<Activities> act = activitiesService.findById(activities);
            hasActivities = act.map(Set::of).orElse(Collections.emptySet());
            destinationsList = destinationsService.getDestinationsByActivity(hasActivities);
        } else {
            Optional<Geography> geo = geographyService.findById(geography);
            hasGeography = geo.map(Set::of).orElse(Collections.emptySet());
            Optional<Activities> act = activitiesService.findById(activities);
            hasActivities = act.map(Set::of).orElse(Collections.emptySet());
            destinationsList = destinationsService.getDestinations(hasGeography, hasActivities);
        }

        return destinationsList.isEmpty() ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(destinationsList);

    }

    @GetMapping("/destinations/{city}")
    public ResponseEntity<DestinationsList> getDestinations(@PathVariable String city) {
        DestinationsList destinationsList = destinationsService.getDestinations(city);

        return destinationsList.isEmpty() ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(destinationsList);
    }

    @PostMapping("/destinations")
    public ResponseEntity<Destinations> addDestinations(@RequestBody Destinations destinations) {

        try {
            destinationsService.addDestinations(destinations);
        } catch (DestinationNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.accepted().build();
    }

    @PatchMapping("/destinations/{city}")
    public ResponseEntity<Destinations> updateDestinations(@PathVariable String city,
                                                           @RequestBody UpdateAttributesRequest update){
        Destinations destinations = destinationsService.updateDestinations(city, update.getGeography(), update.getActivities());
        if(destinations != null){
        destinations.setHasGeography(update.getGeography());
        destinations.setHasActivities(update.getActivities());}

        return destinations==null ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(destinations);
    }

    @DeleteMapping("/destinations/{city}")
    public ResponseEntity deleteDestination(@PathVariable String city) {
        try {
            destinationsService.deleteDestination(city);
        } catch (DestinationNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.accepted().build();
    }


    //---------------------------
    //EXCEPTION HANDLERS
    //---------------------------

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void invalidDestinationsExceptionHandler(InvalidDestinationException e){

    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void destinationsNotFoundExceptionHandler(DestinationNoContent e) {

    }
}
