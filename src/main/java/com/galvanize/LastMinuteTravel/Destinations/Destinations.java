package com.galvanize.LastMinuteTravel.Destinations;

import com.galvanize.LastMinuteTravel.Activities.Activities;
import com.galvanize.LastMinuteTravel.Geography.Geography;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Set;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "destinations")
public class Destinations {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String state;
    private String city;
    @ManyToMany
    @JoinTable(name = "geojoindest",
            joinColumns = @JoinColumn(name = "city_id"),
            inverseJoinColumns = @JoinColumn(name = "geo_id"))
    Set<Geography> hasGeography;
    @ManyToMany
    @JoinTable(name = "actjoindest",
            joinColumns = @JoinColumn(name = "city_id"),
            inverseJoinColumns = @JoinColumn(name = "act_id"))
    Set<Activities> hasActivities;
    private int budget;

//=====================
// CONSTRUCTORS
//=====================

    public Destinations(){
    }

    public Destinations(String state, String city, Set<Geography> hasGeography, Set<Activities> hasActivities, int budget) {
        this.state = state;
        this.city = city;
        this.hasGeography = hasGeography;
        this.hasActivities = hasActivities;
        this.budget = budget;
    }

//=====================
// METHODS
//=====================

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Set<Geography> getHasGeography() {
        return hasGeography;
    }

    public void setHasGeography(ArrayList<String> hasGeography) {

        for (String geo: hasGeography) {
            Geography newGeography = new Geography(geo);
            this.hasGeography.add(newGeography);
        }
    }

    public Set<Activities> getHasActivities() {
        return hasActivities;
    }

    public void setHasActivities(ArrayList<String> hasActivities) {

        for (String act: hasActivities) {
            Activities newActivities = new Activities(act);
            this.hasActivities.add(newActivities);
        }
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void addGeography(String geography) {
        Geography newGeography = new Geography(geography);
        this.hasGeography.add(newGeography);
    }

    public void addActivities(String activities) {

        Activities newActivities = new Activities(activities);
        this.hasActivities.add(newActivities);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
