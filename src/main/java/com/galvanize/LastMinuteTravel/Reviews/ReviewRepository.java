package com.galvanize.LastMinuteTravel.Reviews;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {

    List<Review> findReviewsByCity(Long city);

    List<Review> findReviewsByUser(String user);

    List<Review> findReviewsByRating(double rating);

    Optional<Review> findReviewsById(Long id);
}
