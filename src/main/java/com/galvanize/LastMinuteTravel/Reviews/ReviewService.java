package com.galvanize.LastMinuteTravel.Reviews;

import com.galvanize.LastMinuteTravel.Destinations.DestinationNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReviewService {

    ReviewRepository reviewRepository;

//=====================
// CONSTRUCTORS
//=====================

    public ReviewService(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

//=====================
// METHODS
//=====================

    public ReviewList getReviews() {
        return new ReviewList(reviewRepository.findAll());
    }

    public ReviewList getReviewsByCity(Long city) {
        List<Review> reviews = reviewRepository.findReviewsByCity(city);
        if (!reviews.isEmpty()) {
            return new ReviewList(reviews);
        }
        return null;
    }

    public ReviewList getReviewsByUser(String user) {
        List<Review> reviews = reviewRepository.findReviewsByUser(user);
        if (!reviews.isEmpty()) {
            return new ReviewList(reviews);
        }
        return null;
    }

    public ReviewList getReviewsByRating(Double rating) {
        List<Review> reviews = reviewRepository.findReviewsByRating(rating);
        if (!reviews.isEmpty()) {
            return new ReviewList(reviews);
        }
        return null;
    }

    public void addReviewByCity(Review review) {
        reviewRepository.save(review);
    }

    public Review updateReview(Long id, String comment, double rating) {
        Optional<Review> oReview = reviewRepository.findReviewsById(id);
        if(oReview.isPresent()){
            Review review = oReview.get();
            review.setComment(comment);
            review.setRating(rating);
            return reviewRepository.save(oReview.get());
        }
        return null;
    }

    public void deleteReview(Long id) {
        Optional<Review> oReview = reviewRepository.findReviewsById(id);
        if(oReview.isPresent()){
            reviewRepository.delete(oReview.get());
        }else{
            throw new DestinationNotFoundException();
        }
    }
}