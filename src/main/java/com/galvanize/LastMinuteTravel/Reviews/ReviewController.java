package com.galvanize.LastMinuteTravel.Reviews;

import com.galvanize.LastMinuteTravel.Destinations.DestinationNotFoundException;
import com.galvanize.LastMinuteTravel.Destinations.Destinations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ReviewController {

    ReviewService reviewService;

    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping("/review/destination/{city}")
    public ResponseEntity<ReviewList> getReviewsByCity(@PathVariable Long city) {

        ReviewList reviewList;
        if (city == 0) {
            reviewList = reviewService.getReviews();
        } else {
            reviewList = reviewService.getReviewsByCity(city);
        }

        return reviewList.isEmpty() ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(reviewList);
    }

    @GetMapping("/review/user/{user}")
    public ResponseEntity<ReviewList> getReviewsByUser(@PathVariable String user) {

        ReviewList reviewList;
        if (user == null) {
            reviewList = reviewService.getReviews();
        } else {
            reviewList = reviewService.getReviewsByUser(user);
        }

        return reviewList.isEmpty() ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(reviewList);
    }

    @GetMapping("/review/rating/{rating}")
    public ResponseEntity<ReviewList> getReviewsByRating(@PathVariable Double rating) {

        ReviewList reviewList;
        if (rating == null) {
            reviewList = reviewService.getReviews();
        } else {
            reviewList = reviewService.getReviewsByRating(rating);
        }

        return reviewList.isEmpty() ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(reviewList);
    }

    @PostMapping("/review/destination/")
    public ResponseEntity<Review> addReviewByCity(@RequestBody Review review) {

        try {
            reviewService.addReviewByCity(review);
        } catch (DestinationNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.accepted().build();
    }

    @PatchMapping("review/update/{id}")
    public ResponseEntity<Review> updateReviewByRatingId(@PathVariable Long id,
                                                         @RequestBody Review update) {
        Review newReview = reviewService.updateReview(id, update.getComment(), update.getRating());
        if (newReview != null) {
            newReview.setComment(update.getComment());
            newReview.setRating(update.getRating());
        }

        return newReview==null ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(newReview);
    }

    @DeleteMapping("/review/delete/{id}")
    public ResponseEntity deleteReview(@PathVariable Long id) {
        try {
            reviewService.deleteReview(id);
        } catch (DestinationNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.accepted().build();
    }
}