package com.galvanize.LastMinuteTravel.Reviews;

import com.galvanize.LastMinuteTravel.Destinations.Destinations;
import com.galvanize.LastMinuteTravel.Destinations.DestinationsList;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ReviewList {

    private List<Review> reviews;

//=====================
// CONSTRUCTORS
//=====================

    public ReviewList() {
        this.reviews = new ArrayList<>();
    }
    public ReviewList(List<Review> reviews) {
        this.reviews = reviews;
    }

//=====================
// METHODS
//=====================

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public boolean isEmpty(){
        return this.reviews.isEmpty();
    }

    @Override
    public String toString() {
        return "DestinationsList{" +
                "destinations=" + reviews +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReviewList reviewList = (ReviewList) o;
        return Objects.equals(reviews, reviewList.reviews);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reviews);
    }
}