package com.galvanize.LastMinuteTravel.Reviews;

import javax.persistence.*;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "reviews")
public class Review {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double rating;
    private Long city;
    @Column(name = "review")
    private String comment;
    @Column(name = "author")
    private String user;

//=====================
// CONSTRUCTORS
//=====================

    public Review() {
    }

    public Review(double rating, String comment, String user, Long city) {
        this.rating = rating;
        this.comment = comment;
        this.user = user;
        this.city = city;
    }

//=====================
// METHODS
//=====================


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getCity() {
        return city;
    }

    public void setCity(Long city) {
        this.city = city;
    }
}