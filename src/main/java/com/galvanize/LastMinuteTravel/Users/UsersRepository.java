package com.galvanize.LastMinuteTravel.Users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {

    //Users findUserName(String userName);
    Optional<Users> findByEmail(String email);
    Optional<Users> findByUserName(String userName);
}
