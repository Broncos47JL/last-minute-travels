package com.galvanize.LastMinuteTravel.Users;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class UpdateAttributesRequest {
    private ArrayList<String> geography;
    private ArrayList<String> activities;

    public ArrayList<String> getGeography() {
        return geography;
    }

    public void setGeography(ArrayList<String> geography) {
        this.geography = geography;
    }

    public ArrayList<String> getActivities() {
        return activities;
    }

    public void setActivities(ArrayList<String> activities) {
        this.activities = activities;
    }

}
