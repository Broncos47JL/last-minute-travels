package com.galvanize.LastMinuteTravel.Users;

import com.galvanize.LastMinuteTravel.Destinations.DestinationNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsersService {
    UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public void addUsers(Users users) {
        usersRepository.save(users);
    }

//    public List<Users> getUsers() {
//        return usersRepository.findAll();
//    }

    public Optional<Users> getUserName(String userName) {
        return usersRepository.findByUserName(userName);
    }

    public Optional<Users> getUserByEmail(String email) { return usersRepository.findByEmail(email);}

    public Users updateUsers(String userName, String email, String name) {
        Optional<Users> oUsers = usersRepository.findByUserName(userName);
        if (oUsers.isPresent()) {
            Users users = oUsers.get();
            users.setEmail(email);
            users.setName(name);
            return usersRepository.save(oUsers.get());
        }
        return null;
    }

    public void deleteUsers(String userName) {
        Optional<Users> oUsers = usersRepository.findByUserName(userName);
        if (oUsers.isPresent()) {
            usersRepository.delete(oUsers.get());
        } else {
            throw new DestinationNotFoundException();
        }
    }
}
