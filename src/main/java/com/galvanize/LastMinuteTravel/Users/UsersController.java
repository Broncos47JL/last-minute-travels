package com.galvanize.LastMinuteTravel.Users;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class UsersController {
    UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @PostMapping("/users/new")
    public ResponseEntity<Users> addUsers(@RequestBody Users users) {
        try {
            usersService.addUsers(users);
        } catch (UsersNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.accepted().build();
    }

    @GetMapping("/users/{userName}")
    public ResponseEntity<Optional<Users>> getUsers(@PathVariable String userName) {
        Optional<Users> users = usersService.getUserName(userName);

        return users == null ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(users);
    }

    @GetMapping("/users")
    public ResponseEntity<Optional<Users>> getUserByEmail(@RequestParam String email) {
        Optional<Users> user = usersService.getUserByEmail(email);
        return user == null ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(user);
    }

    @PatchMapping("/users/{userName}")
    public ResponseEntity<Users> updateUsers(@PathVariable String userName,
                                             @RequestBody UpdateUserRequest updateUser) {
        Users users = usersService.updateUsers(userName, updateUser.getEmail(), updateUser.getName());
        if (users != null) {
            users.setName(updateUser.getName());
            users.setEmail(updateUser.getEmail());
        }

        return users == null ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(users);
    }

    @DeleteMapping("/users/{userName}")
    public ResponseEntity<Users> deleteUsers(@PathVariable String userName){
        try {
            usersService.deleteUsers(userName);
        } catch (UsersNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
    return ResponseEntity.accepted().build();
    }

//--------------------Exception----------------------
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void usersNotFoundExceptionHandler(UsersNoContent e) {

    }
}

