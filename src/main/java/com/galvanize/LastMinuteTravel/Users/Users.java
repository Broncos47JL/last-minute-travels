package com.galvanize.LastMinuteTravel.Users;

import javax.persistence.*;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "users")
public class Users {
    @Id
    private String userName;
    private String name;
    private String email;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public Users() {
    }

    public Users(String userName, String name, String email){
        this.userName = userName;
        this.name = name;
        this.email = email;
    }
}