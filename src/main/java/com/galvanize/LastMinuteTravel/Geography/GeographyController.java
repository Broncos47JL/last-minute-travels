package com.galvanize.LastMinuteTravel.Geography;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class GeographyController {

    GeographyService geographyService;

    public GeographyController(GeographyService geographyService) {
        this.geographyService = geographyService;
    }
    @GetMapping("/geography")
    public ResponseEntity<GeographyList> getGeography() {
        GeographyList geographyList;
        geographyList = geographyService.getGeography();
        return geographyList.isEmpty() ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(geographyList);
    }
}