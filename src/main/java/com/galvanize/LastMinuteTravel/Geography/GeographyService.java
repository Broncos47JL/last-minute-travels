package com.galvanize.LastMinuteTravel.Geography;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GeographyService {

    GeographyRepository geographyRepository;

    public GeographyService(GeographyRepository geographyRepository) {
        this.geographyRepository = geographyRepository;
    }

    public GeographyList getGeography() {
        return new GeographyList(geographyRepository.findAll());
    }

    public Optional<Geography> findById(Long geography) {
        return geographyRepository.findById(geography);
    }
}
