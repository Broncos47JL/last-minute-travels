package com.galvanize.LastMinuteTravel.Geography;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GeographyList {

    private List<Geography> geography;

//=====================
// CONSTRUCTORS
//=====================

    public GeographyList() {
        this.geography = new ArrayList<>();
    }
    public GeographyList(List<Geography> geography) {
        this.geography = geography;
    }

//=====================
// METHODS
//=====================

    public List<Geography> getGeography() {
        return geography;
    }

    public void setGeography(List<Geography> geography) {
        this.geography = geography;
    }

    public boolean isEmpty(){
        return this.geography.isEmpty();
    }

    @Override
    public String toString() {
        return "GeographyList{" +
                "geography=" + geography +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GeographyList geographyList = (GeographyList) o;
        return Objects.equals(geography, geographyList.geography);
    }

    @Override
    public int hashCode() {
        return Objects.hash(geography);
    }
}
