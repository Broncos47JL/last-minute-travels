package com.galvanize.LastMinuteTravel.Geography;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.galvanize.LastMinuteTravel.Destinations.Destinations;

import javax.persistence.*;
import java.util.Set;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "geography")

public class Geography {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String geography;
    @ManyToMany(mappedBy = "hasGeography")
    private Set<Destinations> has;

    public Geography(long id, String geography) {
        this.id = id;
        this.geography = geography;
    }

    public Geography(){}

    public Geography(String geography) {
        this.geography = geography;
    }

    public String getGeography() {
        return geography;
    }

    public void setGeography(String geography) {
        this.geography = geography;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
