package com.galvanize.LastMinuteTravel.Geography;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GeographyRepository extends JpaRepository <Geography, Long> {
}
