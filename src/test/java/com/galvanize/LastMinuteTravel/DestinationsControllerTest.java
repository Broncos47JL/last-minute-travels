package com.galvanize.LastMinuteTravel;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.galvanize.LastMinuteTravel.Activities.Activities;
import com.galvanize.LastMinuteTravel.Destinations.*;
import com.galvanize.LastMinuteTravel.Geography.Geography;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.Set;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DestinationsController.class)
public class DestinationsControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    DestinationsService destinationsService;
    ObjectMapper mapper = new ObjectMapper();


//===============================
// GET : /api/destinations
//===============================

    //---------------------------
    // GET : /api/destinations : returns list of all destinations in database
    //---------------------------
    @Test
    public void getDestinatioins_noParams_exists_returnsDestinationsList() throws Exception {
        //Setup
        Geography geo1 = new Geography("mountains");
        Geography geo2 = new Geography("desert");
        Activities act1 = new Activities("Indoor");
        Activities act2 = new Activities("Outdoor");
        List<Destinations> destinations = new ArrayList<>();
        Set<Geography> geography = new HashSet<>();
        geography.add(geo1);
        geography.add(geo2);
        Set<Activities> activities = new HashSet<>();
        activities.add(act1);
        activities.add(act2);


        destinations.add(new Destinations("USA", "San-Antonio", geography, activities, 1));
        destinations.add(new Destinations("France", "Paris", geography, activities, 3));
        destinations.add(new Destinations("Germany", "Berlin", geography, activities, 2));

        //Enact
        when(destinationsService.getDestinations()).thenReturn(new DestinationsList(destinations));

        //Assert
        mockMvc.perform(get("/api/destinations"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.destinations", hasSize(3)));
    }

    //---------------------------
    // GET : /api/destinations : no destinations in database : returns 204 - no content
    //---------------------------

    @Test
    void getDestinations_noParams_none_returnsNoContent() throws Exception {
        //SETUP
        when(destinationsService.getDestinations()).thenReturn(new DestinationsList());
        //ENACT
        mockMvc.perform(get("/api/destinations"))
                .andDo(print())
                //ASSERT
                .andExpect(status().isNoContent());

    }

    //---------------------------
    // GET : /api/destinations?geography=MOUNTAIN&activities=OUTDOORS returns MOUNTAIN OUTDOORS destinations
    //---------------------------

    @Test
    void getDestinations_searchTwoParms_returnsDestinationsList() throws Exception {
        //SETUP
        List<Destinations> destinations = new ArrayList<>();
        Set<Geography> geography = new HashSet<>();
        Set<Activities> activities = new HashSet<>();
        Geography geo1 = new Geography("mountains");
        Geography geo2 = new Geography("desert");
        Activities act1 = new Activities("Indoor");
        Activities act2 = new Activities("Outdoor");
        geography.add(geo1);
        geography.add(geo2);
        activities.add(act1);
        activities.add(act2);

        destinations.add(new Destinations("USA", "San-Antonio", geography, activities, 1));
        destinations.add(new Destinations("France", "Paris", geography, activities, 3));
        destinations.add(new Destinations("Germany", "Berlin", geography, activities, 2));

        when(destinationsService.getDestinations(anySet(), anySet())).thenReturn(new DestinationsList(destinations));
        //ENACT
        mockMvc.perform(get("/api/destinations?hasGeography={\"geography\" : \"mountains\"}&hasActivities={\"activities\":\"activites\"}"))
                .andDo(print())
                //ASSERT
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.destinations", hasSize(3)));

    }
//    @Test
//    void getDestinations_searchGeographyParms_returnsDestinationsList() throws Exception {
//        //SETUP
//        List<Destinations> destinations = new ArrayList<>();
//        ArrayList<String> geography = new ArrayList<>();
//        ArrayList<String> activities = new ArrayList<>();
//        geography.add("MOUNTAINS");
//        geography.add("DESERT");
//        activities.add("OUTDOORS");
//        activities.add("INDOORS");
//
//
//        destinations.add(new Destinations("USA", "San-Antonio", geography, activities, "$", 4.0));
//        destinations.add(new Destinations("France", "Paris", geography, activities, "$$$", 1.0));
//        destinations.add(new Destinations("Germany", "Berlin", geography, activities, "$$", 4.0));
//
//        when(destinationsService.getDestinationsByGeography(anyString())).thenReturn(new DestinationsList(destinations));
//        //ENACT
//        mockMvc.perform(get("/api/destinations?geography=MOUNTAINS"))
//                .andDo(print())
//                //ASSERT
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.destinations", hasSize(3)));
//
//    }
//    @Test
//    void getDestinations_searchActivitiesParms_returnsDestinationsList() throws Exception {
//        //SETUP
//        List<Destinations> destinations = new ArrayList<>();
//        ArrayList<String> geography = new ArrayList<>();
//        ArrayList<String> activities = new ArrayList<>();
//        geography.add("MOUNTAINS");
//        geography.add("DESERT");
//        activities.add("OUTDOORS");
//        activities.add("INDOORS");
//
//
//        destinations.add(new Destinations("USA", "San-Antonio", geography, activities, "$", 4.0));
//        destinations.add(new Destinations("France", "Paris", geography, activities, "$$$", 1.0));
//        destinations.add(new Destinations("Germany", "Berlin", geography, activities, "$$", 4.0));
//
//        when(destinationsService.getDestinationsByActivity(anyString())).thenReturn(new DestinationsList(destinations));
//        //ENACT
//        mockMvc.perform(get("/api/destinations?activities=OUTDOORS"))
//                .andDo(print())
//                //ASSERT
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.destinations", hasSize(3)));
//
//    }
//
//    @Test
//    void getDestinations_searchCityParms_returnsDestinationsList() throws Exception {
//        //SETUP
//        List<Destinations> destinations = new ArrayList<>();
//        ArrayList<String> geography = new ArrayList<>();
//        ArrayList<String> activities = new ArrayList<>();
//        geography.add("MOUNTAINS");
//        geography.add("DESERT");
//        activities.add("OUTDOORS");
//        activities.add("INDOORS");
//
//
//        destinations.add(new Destinations("USA", "San-Antonio", geography, activities, "$", 4.0));
//        destinations.add(new Destinations("France", "Paris", geography, activities, "$$$", 1.0));
//        destinations.add(new Destinations("Germany", "Berlin", geography, activities, "$$", 4.0));
//
//        when(destinationsService.getDestinations(anyString())).thenReturn(new DestinationsList(destinations));
////        ENACT
//        mockMvc.perform(get("/api/destinations/San-Antonio"))
//                .andDo(print())
//                //ASSERT
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.destinations", hasSize(3)));
//    }
//
//    @Test
//    void getDestination_withCity_returns204_destinationNoContent() throws Exception {
//        ArrayList<String> geography = new ArrayList<>();
//        ArrayList<String> activities = new ArrayList<>();
//        geography.add("MOUNTAINS");
//        geography.add("DESERT");
//        activities.add("OUTDOORS");
//        activities.add("INDOORS");
//
//        Destinations destinations = new Destinations("USA","San-Antonio", geography, activities, "$", 4.0);
//
//        when(destinationsService.getDestinations(anyString())).thenThrow(DestinationNoContent.class);
//        mockMvc.perform(get("/api/destinations/" + destinations.getCity()))
//                .andExpect(status().isNoContent());
//
//    }
//
////===============================
//// POST : /api/destinations
////===============================
//
//    //---------------------------
//    // POST : /api/destinations : returns created destinations
//    //---------------------------
//    @Test
//    void addDestinations_valid_returnsDestinations() throws Exception {
//
//        ArrayList<String> geography = new ArrayList<>();
//        ArrayList<String> activities = new ArrayList<>();
//        geography.add("MOUNTAINS");
//        activities.add("OUTDOORS");
//
//        Destinations destinations = new Destinations("USA", "San-Antonio", geography, activities, "$", 4.0);
//        when(destinationsService.addDestinations(any(Destinations.class))).thenReturn(destinations);
//        //ENACT
//        mockMvc.perform(post("/api/destinations").contentType(MediaType.APPLICATION_JSON)
//                        .content(mapper.writeValueAsString(destinations)))
//                .andDo(print())
//                .andExpect(status().isAccepted());
//    }

    //---------------------------
    // POST : /api/destinations : returns error 400 Bad Request
    //---------------------------

    @Test
    void addDestinations_badRequest_returns400() throws Exception {
        when(destinationsService.addDestinations(any(Destinations.class))).thenThrow(InvalidDestinationException.class);
        String json = "{\"country\":USA,\"city\":San-Antonio\",\"geography\":{MOUNTAINS}\",\"activities\":{OUTDOORS},\"budget\":$,\"reviews \":4.0\"}";
        mockMvc.perform(post("/api/destinations").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(print())
                .andExpect(status().isBadRequest());

    }

    //---------------------------
    // PATCH : /api/destinations : returns created destinations
    //---------------------------

//    @Test
//    void updateDestinationsWithObjectReturnsDestinations() throws Exception {
//        ArrayList<String> geography = new ArrayList<>();
//        ArrayList<String> activities = new ArrayList<>();
//        geography.add("MOUNTAINS");
//        activities.add("OUTDOORS");
//
////        ArrayList<String> updateGeography = new ArrayList<>();
////        ArrayList<String> updateActivities = new ArrayList<>();
////        updateGeography.add("RIVER");
////        updateActivities.add("INDOORS");
//
//
//        Destinations destinations = new Destinations("USA", "San-Antonio", geography, activities, "$", 4.0);
//        when(destinationsService.updateDestinations(anyString(), mockArrayList1, mockArrayList2)).thenReturn(destinations);
//        mockMvc.perform(patch("/api/destinations/" + destinations.getCity())
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content("{\"country\":USA,\"city\":San-Antonio\",\"geography\":{MOUNTAINS, RIVER}\",\"activities\":{OUTDOORS, INDOORS},\"budget\":$,\"reviews \":4.0\"}"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("geography").value("{MOUNTAINS, RIVER}"))
//                .andExpect(jsonPath("activities").value("{OUTDOORS, INDOORS}"));
//
//    }


}

