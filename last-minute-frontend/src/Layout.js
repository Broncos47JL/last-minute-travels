import React from 'react';
import NavbarWrapper from './NavbarWrapper';

const Layout = ({ children }) => {
  return (
    <div>
      <NavbarWrapper />
      <div className="container">{children}</div>
    </div>
  );
};

export default Layout;
