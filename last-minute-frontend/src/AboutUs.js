import './about.css';
import { useEffect, useState } from 'react';
import {Link} from "react-router-dom"
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import withNavbar from './withNavbar';


function AboutUs() {
    return(
        <div className="about-section">
          <h1>About Us</h1>
          <p>Our awesome team that created the Last Minute Travel App is dedicated to helping
            <br></br> spontaneous and last minute travelers find destinations based on two simple search criteria:
            <br></br> What do you want to do, and what do you want to see?
            <br></br>
            <br></br>Meet our extraordinary developers!
            <br></br>
            </p>


        <h2>Our Team</h2>
        <div class="row">
          <div className="column">
            <div className="card">
              <img src="./images/alicia.jpg" alt="Alicia"/>
              <div className="container">
                <h2>Jane Doe</h2>
                <p className="title">Alicia Ramirez</p>
                <p style ={{ color: 'black'}}><b>USAA Experience:</b> Senior Property Adjuster with USAA for 5 years
                                               <br></br><b>Training:</b> Galvanize Certified
                                               <br></br><b>Superpower:</b> Building Databases and Attention to detail</p>

              </div>
            </div>
          </div>

          <div className="column">
            <div className="card">
              <img src="./images/jon.png" alt="Jon"/>
              <div className="container">
                <h2>Mike Ross</h2>
                <p className="title">Jon Nussbaum</p>
                <p style ={{ color: 'black'}}><b>USAA Experience:</b> Banking Sales and Service Representative been with USAA for 6 years
                                              <br></br><b>Training:</b> Galvanize Certified
                                              <br></br><b>Superpower:</b> Logo Design</p>


              </div>
            </div>
          </div>

          <div className="column">
            <div className="card">
              <img src="./images/mike.png" alt="Mike"/>
              <div className="container">
                <h2>John Doe</h2>
                <p className="title">Mike Kallo</p>
                <p style ={{ color: 'black'}}><b>USAA Experience:</b> 10 Years in P&C started as an Insurance Rep and promoted to Manager
                                            <br></br><b>Training:</b> Galvanize Certified
                                            <br></br><b>Superpower:</b> Working some JPA Repository magic</p>

              </div>
            </div>
          </div>

            <div className="column">
              <div className="card">
                <img src="./images/nathan.png" alt="Nathan"/>
                <div className="container">
                  <h2>John Doe</h2>
                  <p className="title">Nathan Breazeal</p>
                  <p style ={{ color: 'black'}}><b>USAA Experience:</b> Bank Sales and Service Rep specializing in Disputes
                                              <br></br><b>Training:</b> Galvanize Certified
                                              <br></br><b>Superpower:</b> Researching Front-End Syntax</p>

                </div>
              </div>
            </div>

          <div className="column">
            <div className="card">
              <img src="./images/steven.jpg" alt="Steven"/>
              <div className="container">
                <h2>John Doe</h2>
                <p className="title">Steven DeNoia</p>
                <p style ={{ color: 'black'}}><b>USAA Experience: </b>Started with the summer surge and transfered to the Agency
                                            <br></br><b>Training:</b> Galvanize Certified
                                            <br></br><b>Superpower:</b> Cleaning up code and Hosting a Postgres database</p>

              </div>
            </div>
          </div>
        </div>
        
        </div>
);
}

export default withNavbar(AboutUs);