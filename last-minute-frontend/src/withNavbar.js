import React from 'react';
import NavbarWrapper from './NavbarWrapper';

const withNavbar = (WrappedComponent) => {
  return () => (
    <div>
      <NavbarWrapper />
      <WrappedComponent />
    </div>
  );
};

export default withNavbar;