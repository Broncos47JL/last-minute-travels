import './App.css';
import { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import {Link} from "react-router-dom"
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import Nav from 'react-bootstrap/Nav';
import withNavbar from './withNavbar';


function Home() {

  const [inputGeo, setInputGeo] = useState("");
  const [inputAct, setInputAct] = useState("");
  const [destinationDisplay, setDestinationDisplay] = useState([]);
  const [geographySet, setGeography] = useState([]);

  useEffect(() => { getGeography() }, []);
  const getGeography = () => {
    fetch("http://localhost:8080/api/geography")
      .then(response => response.json())
      .then(data => {
        setGeography(data.geography);
        let newArray = data.geography.map(geography => <option value={geography.id}>{geography.geography}</option>);
        setGeography(newArray);
      });
  }
  const [activitiesSet, setActivities] = useState([]);
  useEffect(() => { getActivities() }, []);
  const getActivities = () => {
    fetch("http://localhost:8080/api/activities")
      .then(response => response.json())
      .then(data => {
        setActivities(data.activities);
        let newArray = data.activities.map(activities => <option value={activities.id}>{activities.activity}</option>);
        setActivities(newArray);
      });
  }
  const getDestination = () => {
    setDestinationDisplay([]);
    
    fetch("http://localhost:8080/api/destinations" + createGeoFetch() + createActFetch())
      .then(response => response.json())
      .then(data => {
        let newArray = []
        for(let i=0; i<data.destinations.length; i++) {
          let destinationObject = {}
          destinationObject.city = data.destinations[i].city;
          destinationObject.state = data.destinations[i].state;
          fetch("https://api.openweathermap.org/data/2.5/weather?q=" + data.destinations[i].city + "&appid=cb36a9d39d6af33dbf2966b32abd004d&units=Imperial")
            .then(response => response.json())
            .then((weatherData) => {
              destinationObject.conditions = weatherData.weather[0].icon;
              destinationObject.temp = Math.round(weatherData.main.temp);
              newArray.push(destinationObject)
              if (i === data.destinations.length - 1) {
                createDestinations(newArray);
              }
            });
        }
        });
      };

  const createDestinations = (destinationSet) => {
    let destinationArray = destinationSet.map(destination => {

      const imageUrl = `/images/${destination.city}.jpg`
      const imageSrc = `https://openweathermap.org/img/wn/${destination.conditions}@2x.png`
    
      return (
          <Card className="card custom-bg" style={{ width: '14rem', margin: '10px'}}>
            <Card.Img variant="top" src={imageUrl}
            style={{ width: "100%", height: "100%" }} 
            />
            <Card.Body>
              <Card.Title>{destination.city}</Card.Title>
              <Card.Subtitle >{destination.state}</Card.Subtitle>
              <Card.Text>{destination.temp} °F{imageSrc && <Image src={imageSrc} width="24" height="24" />}</Card.Text>
              <Button as={Link} to="/usersAndReviews"variant="primary" size="sm">Leave a Review</Button>
            </Card.Body>
          </Card>
      );
    });
    console.log(destinationDisplay)
    setDestinationDisplay(destinationArray);
    console.log(destinationDisplay)
  }
  
  const handleSubmit = () => {
    getDestination();
  }

  const createGeoFetch = () => {
    if (inputGeo !== "") {
      return "?geography=" + inputGeo;
    } else {
      return "";
    }
  }
  const createActFetch = () => {
    if (inputAct !== "" && inputGeo !== "") {
      return "&activities=" + inputAct;
    } else if (inputAct !== "" && inputGeo === "") {
      return "?activities=" + inputAct;
    } else {
      return "";
    }
  }

  const homeImage = '/logo-transparent.png'


  return (
    <div className="App" style={{ display: 'flex', flexDirection: 'column', minHeight: '93vh' }}>
      <div className="homepage-container">
      <img src='/images/logo-white.png' /> 
      <h5>Choose your scenery:</h5>
      <form onSubmit={(e) => {
        e.preventDefault()
        handleSubmit()
      }}>
        <select onChange={(e) => {
          setInputGeo(e.target.value)
        }}>
          <option></option>
          {geographySet}
        </select>
        <h5>Choose your adventure:</h5>
        <select onChange={(e) => {
          setInputAct(e.target.value)
        }}>
          <option></option>
          {activitiesSet}
        </select> <br />
        <Button type="submit" variant="primary" size="sm">Find Destination</Button><br></br>
        <h3>Destinations:</h3>
        <br />
        </form>
        </div>
        <Container id="card-container" style={{ flex: '1' }}> {/* Added justifyContent property */}
        <Row className="justify-content-center">
          {destinationDisplay.length > 0 ? (
            destinationDisplay // Render the destinationDisplay variable directly
            ) : (<p>No destinations found.</p>
            )}
          </Row>
        </Container>
       

        <footer className='gradient-container' style={{ flexShrink: 0, padding: '40px', textAlign: 'center' }}>
        <Container fluid>
          <Row>
            <Container>
              <p className="mb-0"> Last Minute Travelers - Galvanize - Capstone Project - 2023 Ⓒ
              </p>
            </Container>
          </Row>
        </Container>
      </footer>
  </div>
  );
}
export default withNavbar(Home);