import './App.css';
import { useEffect, useState } from 'react';
import {Link} from "react-router-dom"
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

const NavbarWrapper = () => {
  // Add your navbar logic here

  return (
    // Navbar component code here

    /* <Nav variant="pills" defaultActiveKey="/home">
    <Nav.Item>
      <Nav.Link href="/">Home</Nav.Link>
    </Nav.Item>
    <Nav.Item>
      <Nav.Link href="/about_us">About Us</Nav.Link>
    </Nav.Item>
    <Nav.Item>
      <Nav.Link href="/usersAndReviews">Reviews</Nav.Link>
    </Nav.Item>
  </Nav> */

<Navbar variant="light">
<Container>
<Navbar.Brand href="#home"><img src='/images/logo-transparent.png' width={80} height={60} />
</Navbar.Brand>
  <Nav className="me-auto" >
    <Nav.Link className="nav-link" href="/">Home</Nav.Link>
    <Nav.Link className="nav-link" href="/about_us">About Us</Nav.Link>
    <Nav.Link className="nav-link" href="/usersAndReviews">Reviews</Nav.Link>
  </Nav>
</Container>
</Navbar>
  );
};
export default NavbarWrapper;