import './App.css';
import { useEffect, useState } from 'react';
import {Link} from "react-router-dom"
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import NavbarWrapper from './NavbarWrapper';
import withNavbar from './withNavbar';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function UsersAndReviews() {

    const [inputRating, setInputRating] = useState("")
    const [inputReview, setInputReview] = useState("")
    const [inputCity, setInputCity] = useState("")
    const [citySet, setCity] = useState([])
    const [inputName, setInputName] = useState("")
    const [inputUsername, setInputUsername] = useState("")
    const [inputEmail, setInputEmail] = useState("")
    const [reviewSet, setReview] = useState([])
    const [reviewDisplay, setReviewDisplay] = useState([])
    const [user, setUser] = useState(null)

    useEffect(() => { getCities() }, []);
  const getCities = () => {
    fetch("http://localhost:8080/api/destinations")
      .then(response => response.json())
      .then(data => {
        setCity(data.destinations.city);
        let newArray = data.destinations.map(city => <option value={city.id}>{city.city}</option>);
        setCity(newArray);
      });
  }

  const getReviews = () => {
    fetch("http://localhost:8080/api/review/destination/"+inputCity)
        .then(response => response.json())
        .then(data => {
            let newArray = data.reviews.map((reviews, index) => {
                let reviewObject = {}
                reviewObject.id = reviews.id
                reviewObject.city = reviews.city;
                reviewObject.user = reviews.user;
                reviewObject.comment = reviews.comment
                reviewObject.rating = reviews.rating
                setReview(prevReviews => [...prevReviews, reviewObject]);
                if (index === data.reviews.length - 1) {
                    createReviews();
                }
        });
  });
}

const deleteReviews = (id, reviewUser) => {
    if(reviewUser != user.userName){
        return(<h5>You cannot delete someone else's review.</h5>)
    } else {
    fetch("http://localhost:8080/api/review/delete/" + id, 
        {method:'DELETE'} )
    }
}

  const createReviews = () => {
    let reviewArray = reviewSet.map(review => {
      
        return (
            <Card className="card custom-bg" style={{ width: '12rem', margin: '10px'}}>
              <Card.Body>
                <Card.Title>{review.user} said:</Card.Title>
                <Card.Text>Comments: {review.comment}</Card.Text>
                <Card.Text>Rating: {review.rating}</Card.Text>
                <Button onClick={() => { deleteReviews(review.id, review.user);}}>
                    Delete Review
                </Button>
              </Card.Body>
            </Card>
        );
      });
      console.log(reviewArray)

      setReviewDisplay(reviewArray);
  }

    const getUser = () => {
        fetch("http://localhost:8080/api/users?email="+inputEmail)
        .then(response => response.json())
        .then(data => {
            let user = data
            setUser(user)
        });
    }

    const createUserResponse = () => {
        return (
            <div>
                <h5>No profile is associated with that email, <br />please try another or enter a username and your name to create a new profile:</h5>
                <form onSubmit = {(e) => {
                    e.preventDefault()
                    createUser()}}>
                        <h5>Username:</h5>
                        <input onChange={(e) => {
                            setInputUsername(e.target.value)
                        }}></input>
                        <h5>Name:</h5>
                        <input onChange = {(e) => {
                            setInputName(e.target.value)
                        }}></input>
                        <br></br>
                        <br></br>
                        <Button type="submit">Create User Profile</Button>
                    </form>
            </div>
        );

    }

    const createUser = () => {
        
        var jsonData = {
                "userName": inputUsername,
                "name": inputName,
                "email": inputEmail
        }
        
        fetch("http://localhost:8080/api/users/new", {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(jsonData)
        })

    }

    const handleSubmit = () => {
        getReviews();
    }

    const reviewResponse = () => {

        return (
            <div>
                
                <h5>Welcome Back!</h5>
                <br></br>
                <h4>Create a Review</h4>
                <h5>Please select the city you traveled to:</h5>
                <form onSubmit = {(e) => {
                    e.preventDefault()
                    postReview()}}>
                        <select onChange={(e) => {
                        setInputCity(e.target.value)
                        }}>
                            <option></option>
                            {citySet}
                        </select>
                        <h5>Your overall rating of your trip on a scale of 1 to 5:</h5>
                        <select onChange={(e) => {
                            setInputRating(e.target.value)
                        }}>
                            <option></option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                        <h5>Please write a description of your trip:</h5>
                        <input onChange ={(e) => {
                            setInputReview(e.target.value)
                    }}></input>
                    <br></br>
                    <Button type="submit">Submit Review</Button>
                </form>
                <br></br>
                <h4>Read Reviews</h4>
                <h5>Please select the city you would like to read reviews for:</h5>
                <form onSubmit = {(e) => {
                    e.preventDefault()
                    handleSubmit()}}>
                        <select onChange={(e) => {
                        setInputCity(e.target.value)
                        }}>
                            <option></option>
                            {citySet}
                        </select>
                        <Button type="submit">Get Reviews</Button>
                </form>
                <Container id="card-container"> {/* Added justifyContent property */}
        <Row className="justify-content-center">
          {reviewDisplay.length > 0 ? (
            reviewDisplay // Render the destinationDisplay variable directly
            ) : (<p>No reviews found.</p>
            )}
          </Row>
        </Container>
            </div>
        )
    }

    const postReview = () => {
        
        setInputUsername(user.userName.stringify)

        var jsonData = {
            "rating": inputRating,
            "comment": inputReview,
            "user": inputUsername,
            "city": inputCity
    }
    
    fetch("http://localhost:8080/api/review/destination/", {
        method: 'POST',
        mode: 'cors',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(jsonData)
    })
    }

return(
    <div className="App" style={{ display: 'flex', flexDirection: 'column', minHeight: '93vh' }}>
        <div className="reviews-container">
        <img src='/images/logo-white.png' /> 
        <h1>Users and Reviews</h1>
        <h4>To view and write reviews, please sign in or create a profile.</h4>
        <h5>Please enter your email address:</h5>
        <form onSubmit = {(e) => {
            e.preventDefault()
            getUser()}}>
            <input onChange ={(e) => {
                setInputEmail(e.target.value)
            }}></input>
            <Button type="submit">Submit Email</Button>
        </form>
        </div>
        <Container style={{ flex: '1' }}>
        <Row>
        {user != null ? (reviewResponse()
        ) : (createUserResponse()
        )}
        </Row>
        </Container>

        <footer className='gradient-container' style={{ flexShrink: 0, padding: '40px', textAlign: 'center' }}>
        <Container fluid>
          <Row>
            <Container>
              <p className="mb-0"> Last Minute Travelers - Galvanize - Capstone Project - 2023 Ⓒ
              </p>
            </Container>
          </Row>
        </Container>
      </footer>
    </div>
    );
}
export default withNavbar(UsersAndReviews);