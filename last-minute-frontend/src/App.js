import Home from "./Home"
import UsersAndReviews from "./UsersAndReviews";
import AboutUs from "./AboutUs";
import {Route, BrowserRouter as Router, Routes} from "react-router-dom";


function App() {

    return(
        <Router>        
            <div>
                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/usersAndReviews/" element={<UsersAndReviews/>}/>
                    <Route path="/about_us/" element={<AboutUs/>}/>
                </Routes>
            </div>
        </Router>

    )

}

export default App;